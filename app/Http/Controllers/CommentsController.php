<?php

namespace App\Http\Controllers;
use App\Models\Comment;
use App\Models\User;
use App\Models\Tickets;
use App\Mailers\AppMailer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class CommentsController extends Controller
{
    public function postComment(Request $request, AppMailer $mailer)
    {
    	$ticket = Tickets::where('ticket_id',$request->ticket_id)->first();
    	$ticketOwner = User::where('id',$ticket->user_id)->first();
    	$user = User::where('id',$request->input('user_id'))->first();

    	
        if(User::where('id',$request->user_id)->where('is_admin','!=',1)->exists()){
	        $comment = Comment::create([
	            'ticket_id' => $request->input('ticket_id'),
	            'user_id' => $request->input('user_id'),
	            'comment' => $request->input('comment')
	        ]);
	        // send mail if the user commenting is not the ticket owner
	        if($ticket->user_id !== $request->input('user_id')) {
	            $mailer->sendTicketComments($ticketOwner, $user, $ticket, $comment);
	        }

	        return response()->json([
	        	"message" => "Your comment has been submitted"
	        ],201);

		} else {
			return response()->json([
				"message" => "Unauthorized to comment"
			],401);
		}
    }
}