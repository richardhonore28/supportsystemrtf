<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tickets;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Support\Str;
use App\Mailers\AppMailer;

class TicketsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //listing all tickets
        $tickets = Tickets::get()->toJson(JSON_PRETTY_PRINT);
        return response($tickets, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, AppMailer $mailer)
    {
        //
        $user = User::where('id',$request->user_id)->first();

        if(User::where('id',$request->user_id)->where('is_admin','!=',1)->exists()){
            $ticket = new Tickets([
                'title' => $request->input('title'),
                'user_id' => $request->user_id,
                'ticket_id' => strtoupper(Str::random(10)),
                'category_id' => $request->input('category_id'),
                'priority' => $request->input('priority'),
                'message' => $request->input('message'),
                'status' => "Not Answered"
            ]);

            $ticket->save();
          
            $mailer->sendTicketInformation($user, $ticket);

            return response()->json([
                "message" => "A ticket with ID: # $ticket->ticket_id has been opened."
            ], 201);
        } else {
            return response()->json([
                "message" => "Admin is unauthorized to submit a ticket"
            ], 401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if (Tickets::where('ticket_id',$id)->exists()) {
            $ticket = Tickets::where('ticket_id', $id)->get()->toArray();
            if (Comment::where('ticket_id',$id)->exists()) {
                $comment = Comment::where('ticket_id', $id)->get()->toArray();
            }
            else {
                $comment = NULL;
            }
            return response()->json([
                "ticket" => $ticket,
                "comments" => $comment
            ], 200);
        } else {
            return response()->json([
                "message" => "Ticket not found"
            ],404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, AppMailer $mailer)
    {
        //

        $ticket = Tickets::where('ticket_id', $id)->firstOrFail();
        $ticket->status = $request->status;
        $ticket->save();
        $ticketOwner = User::where('id',$ticket->user_id)->first();
        $mailer->sendTicketStatusNotification($ticketOwner, $ticket);

        return response()->json([
            "message" => "Ticket status successfully updated"
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getUserTickets(Request $request){

        if(Tickets::where('user_id', $request->user_id)->exists()){
            $tickets  = Tickets::where('user_id', $request->user_id)->get();
            foreach($tickets as $ticket)
            {
                if(Comment::where('ticket_id', $ticket->ticket_id)->exists()){
                    $ticket->comments = Comment::where('ticket_id', $ticket->ticket_id)->get()->toArray();
                } else {
                    $ticket->comments = NULL;
                }

                
            }
            return response()->json($tickets, 200);
        }
    }
}
